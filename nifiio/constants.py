import re

nse_stock_list_url_dict = {
            "NIFTY Alpha 50" : "https://www.niftyindices.com/IndexConstituent/ind_nifty_Alpha_Index.csv",
            "NIFTY Dividend Opportunities 50" : "https://www.niftyindices.com/IndexConstituent/ind_niftydivopp50list.csv",
            "NIFTY Growth Sectors 15" : "https://www.niftyindices.com/IndexConstituent/ind_NiftyGrowth_Sectors15_Index.csv",
            "NIFTY High Beta 50" : "https://www.niftyindices.com/IndexConstituent/nifty_High_Beta50_Index.csv",
            "NIFTY Low Volatility 50" : "https://www.niftyindices.com/IndexConstituent/nifty_Low_Volatility50_Index.csv",
            "NIFTY Alpha Low-Volatility 30" : "http://www.niftyindices.com/IndexConstituent/ind_nifty_alpha_lowvol30list.csv",
            "NIFTY Quality Low-Volatility 30" : "http://www.niftyindices.com/IndexConstituent/ind_nifty_quality_lowvol30list.csv",
            "NIFTY50 Value 20" : "http://www.niftyindices.com/IndexConstituent/ind_Nifty50_Value20.csv",
            "NIFTY500 VALUE 50" : "http://www.niftyindices.com/IndexConstituent/ind_nifty500Value50_list.csv",
            "NIFTY200 QUALITY 30" : "http://www.niftyindices.com/IndexConstituent/ind_nifty200Quality30_list.csv",
            "NIFTY Alpha Quality Value Low-Volatility 30" : "http://www.niftyindices.com/IndexConstituent/ind_nifty_alpha_quality_value_lowvol30list.csv",
            "NIFTY Alpha Quality Low-Volatility 30" : "http://www.niftyindices.com/IndexConstituent/ind_nifty_alpha_quality_lowvol30list.csv",
            "NIFTY Financial Services" : "http://www.niftyindices.com/IndexConstituent/ind_niftyfinancelist.csv",
            "NIFTY Private Bank" : "http://www.niftyindices.com/IndexConstituent/ind_nifty_privatebanklist.csv"
        } 

nse_weight_regex_pattern = re.compile('"label":"(.*?)"')

stocks_csv_url = 'http://www.nseindia.com/content/equities/EQUITY_L.csv'
index_weight_url = 'http://iislliveblob.niftyindices.com/jsonfiles/SectorialIndex/SectorialIndexData{0}.js'
stock_list_url = 'https://www.niftyindices.com/IndexConstituent/ind_{0}list.csv'
stock_list_url_nse = 'https://www.nseindia.com/content/indices/ind_{0}list.csv'

broad_indices = ['NIFTY 50',
    'NIFTY NEXT 50',
    'NIFTY 100',
    'NIFTY 200',
    'NIFTY 500',
    'NIFTY MIDCAP 150',
    'NIFTY MIDCAP 50',
    'NIFTY MIDCAP 100',
    'NIFTY SMALLCAP 250',
    'NIFTY SMALLCAP 50',
    'NIFTY SMALLCAP 100',
    'NIFTY LARGEMIDCAP 250',
    'NIFTY MIDSMALLCAP 400']
strategy_indices = ['NIFTY100 Low Volatility 30',
    'NIFTY Alpha 50',
    'NIFTY Dividend Opportunities 50',
    'NIFTY Growth Sectors 15',
    'NIFTY High Beta 50',
    'NIFTY Low Volatility 50',
    'NIFTY Alpha Low-Volatility 30',
    'NIFTY Quality Low-Volatility 30',
    'NIFTY Alpha Quality Low-Volatility 30',
    'NIFTY Alpha Quality Value Low-Volatility 30',
    'NIFTY100 QUALITY 30',
    'NIFTY50 Value 20',
    'NIFTY500 VALUE 50']
sectoral_indices = ['NIFTY Auto',
    'NIFTY Bank',
    'NIFTY Financial Services',
    'NIFTY FMCG',
    'NIFTY IT',
    'NIFTY Media',
    'NIFTY Metal',
    'NIFTY Pharma',
    'NIFTY Private Bank',
    'NIFTY PSU Bank',
    'NIFTY Realty']

REQUEST_HEADERS_NSE = {
    'Referer': 'https://www.nseindia.com/index_nse.htm',
    'X-Requested-With': 'XMLHttpRequest',
    'User-Agent':
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36',
    'Accept': '*/*',
    'Host': 'www.nseindia.com'
}

REQUEST_HEADERS_NIF_IND = {
    'Referer': 'https://www.niftyindices.com',
    'X-Requested-With': 'XMLHttpRequest',
    'User-Agent':
    'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36',
    'Accept': '*/*',
    'Host': 'www.niftyindices.com'
}

BOT_HEADERS_NIF_IND = {
     'Referer': 'https://www.niftyindices.com',
    'X-Requested-With': 'XMLHttpRequest',
    'User-Agent': 'Googlebot/2.1 (+http://www.google.com/bot.html)',
    'Accept': '*/*',
    'Host': 'www.niftyindices.com'
}

APP_DIR_NAME = '.nifiio'