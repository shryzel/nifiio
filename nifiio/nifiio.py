"""
MIT License

Copyright (c) 2019 shryzel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
import json, csv
import re
import requests
import os
import time
import constants

# from constants import (nse_stock_list_url_dict, nse_weight_regex_pattern, index_weight_url, stock_list_url, broad_indices, strategy_indices, sectoral_indices)

class Nifiio:
    def __init__(self):
        self.json_ext = '.json'
        self.csv_ext = '.csv'
        self.weightage_field_name = 'Weightage'
        self.sector_field_name = 'Industry'
        
    def get_nse_index_weights(self, index_name):
        """
        Returns a dictionary of all the stocks and their corresponding weights in the index.

        :param index_name: Name of the index
        :rtype: dict
        """
        #prepare and send request to fetch data
        print("Fetching data for index {}".format(index_name))
        url = constants.index_weight_url.format(index_name.upper())
        # print('URL: {}'.format(url))         
        response = requests.get(url, timeout=10)
        print('Request URL: {}'.format(response.url))
        response_text = None
        
        if response.status_code != 200:
            print('Failed to get data: ', response.status_code)
            return None
        else:
            response_text = response.text
            
            #Compile regex and search in response text to fetch 'label:' <weight> from response 
            if response_text is not None:
                print('Response received - parsing')
                # nse_json_regex_pattern = self.nse_weight_regex_pattern
                # pattern = re.compile(nse_json_regex_pattern)
                index_weight_data = constants.nse_weight_regex_pattern.findall(response_text)
                # print(index_weight_data)

                #convert to dict form
                weight_data_dict = dict(st.rsplit(' ', 1) for st in index_weight_data)
                # print(weight_data_dict)
                weight_int_dict =\
                            {key : float(value.replace('%','')) for key, value in weight_data_dict.items()}
                
                return weight_int_dict
            else:
                print('Error: Response is empty!')
                return None
    
    
    def get_nse_index_stocklist(self, index_name):
        """
        Returns a list of stocks in the index as a list of `OrderedDict` types.
        Each `OrderedDict` represents information of a stock in the index. Weights are not included.
        
        :param index_name: Name of the index
        :rtype: OrderedDict
        """
        print("start: get_nse_index_stocklist()")
        #prepare and send request to fetch data
        if index_name in constants.nse_stock_list_url_dict:
            url = constants.nse_stock_list_url_dict.get(index_name)
        else:
            trim_index_name = re.sub(r'\s+', '', index_name)
            print("Fetching list of stocks for index {}".format(index_name))
            url = constants.stock_list_url.format(trim_index_name.lower())
            #print('Request URL: {}'.format(url))
        try:
            print('Request URL: {}'.format(url))
            if "niftyindices" in url:
                request_headers = constants.REQUEST_HEADERS_NIF_IND
            else:
                request_headers = constants.REQUEST_HEADERS_NSE
            print('headers: {}'.format(request_headers))
            response = requests.get(url, headers = request_headers, timeout=10)
            # print(response.text.splitlines())
            response.raise_for_status()
        except requests.exceptions.RequestException as e:
            print(e)
            return

        if response.status_code != 200:
            print('Failed to get data: ', response.status_code)
            return None
        else:
            response_text_split = response.text.splitlines()
            if response is not None:
                print('Response received - reading csv')            
                reader = csv.DictReader(response_text_split)
                stock_list = list(reader)                                
                return stock_list
            else:
                print('Error: Response is empty!')
                return None

    def get_stocks_and_weights(self, index_name, write_to_file=False, file_types: list=None, write_to_file_path=None):
        """Combines fetched stock data together with weights for a more comprehensive list containing details of stocks in the index, each stock represented by an `OrderedDict`.
        Data fetched can also be written to csv and json depending on the optional params.
        
        :param index_name: Name of the index
        :rtype: OrderedDict       
        """
        
        stock_list = self.get_nse_index_stocklist(index_name = index_name)
        weight_dict = self.get_nse_index_weights(index_name = index_name)
        trim_index_name = re.sub(r'\s+', '', index_name)

        if stock_list is None or weight_dict is None:
            print('Couldn\'t fetch stock data and/or weights!')
            return None

        # merge weight from list of weights into stock data using symbol
        print('Merging weights and stock info')
        for stock in stock_list:
            symbol = stock.get('Symbol')
            # print('Getting weight for symbol: {}'.format(symbol))
            weight = weight_dict.get(symbol)
            # print('Weight for {0} is {1}'.format(symbol, weight))
            stock[self.weightage_field_name] = weight
        
        # calculate missing weightage if only single stock is missing its weightage
        missing_weights = [ 
            stock 
            for stock in stock_list 
            if stock['Weightage'] is None
        ]
        # print('missing weights: {}'.format(missing_weights))
        missing_weights_count = len(missing_weights)
        print('missing weights count: {}'.format(missing_weights_count))

        if missing_weights_count == 1:
            weights_available_list = [
                stock 
                for stock in stock_list 
                if stock['Weightage'] is not None
            ]
            missing_weight_stock = self.find_missing_weight(weights_available_list, missing_weights.pop())
            
            if missing_weight_stock:
                weights_available_list.append(missing_weight_stock)
                stock_list = weights_available_list
        
        if write_to_file is True:
            self.write_to_file(stock_list, trim_index_name, write_to_file_path, file_types)
        else:
            print('Skipped writing to file')

        return stock_list

    def write_to_file(self, stock_list: list, trim_index_name: str, write_to_file_path: str, file_types: list):
        if not write_to_file_path:
            write_to_file_path = self.get_default_file_directory('output')
        # write_to_file_path = '' if write_to_file_path is None else str(write_to_file_path)
        if not os.path.isdir(write_to_file_path) and write_to_file_path is not None:
            print('Creating new directory')
            try:
                os.makedirs(write_to_file_path)
            except OSError:
                print('Error creating directory!')
                return

        if 'csv' in file_types:
            # print('Writing to csv file.')
            output_file = os.path.join(write_to_file_path, 'weights_{}'.format(trim_index_name) + self.csv_ext)
            # output_file = write_to_file_path + 'weights_{}'.format(trim_index_name) + self.csv_ext
            print('Writing to csv file. Path: ', output_file)

            #get field names from stock list
            for stock in stock_list[:1]:
                field_names = list(stock.keys())

            with open(output_file, 'w', newline='') as outfile:
                writer = csv.DictWriter(outfile, fieldnames=field_names)
                print('Field names: ', field_names)
                writer.writeheader()
                writer.writerows(stock_list)

        if 'json' in file_types:
            output_file = os.path.join(write_to_file_path, 'weights_{}'.format(trim_index_name) + self.json_ext)
            print('Writing to csv file. Path: ', output_file)
            # print('Writing to json file.')
            # output_file = write_to_file_path + 'weights_{}'.format(trim_index_name) + self.json_ext

            with open(output_file, 'w') as outfile:
                json.dump(stock_list, outfile)

    def get_nse_sector_weights(self, index_name):
        """
        Returns a dictionary of all the sectors and their corresponding weights in the index.

        :param index_name: Name of the index
        :rtype: dict
        """
        stock_list = self.get_nse_index_stocklist(index_name = index_name)
        weight_dict = self.get_nse_index_weights(index_name = index_name)

        if stock_list is None or weight_dict is None:
            print('Couldn\'t fetch sector data and/or weights!')
            return None

        sector_set = set()
        for stock in stock_list:
            sector_set.add(stock.get(self.sector_field_name))

        sector_dict = {}
        for sector in sector_set:
            sector_dict[sector] = weight_dict.get(sector)
        
        # print(sector_dict)
        return sector_dict
    
    def get_indices_for_stock(self, stock_symbol, indices_type='all'):

        indices_containing_stock = []

        if indices_type == 'broad':
            indices_to_scan = constants.broad_indices
        elif indices_type == 'strategy':
            indices_to_scan = constants.strategy_indices
        elif indices_type == 'sectoral':
            indices_to_scan = constants.sectoral_indices
        else:
            indices_to_scan = constants.broad_indices + constants.sectoral_indices \
                + constants.strategy_indices    

        for index in indices_to_scan:
            stock_list = self.get_nse_index_stocklist(index_name = index)
            # weight_dict = self.get_nse_index_weights(index_name = index_name)

            if stock_list is None :
                print('Couldn\'t fetch stock list for index: {}'.format(index))
                continue
                # return None

            for stock in stock_list:
                if stock['Symbol'] == stock_symbol.upper():
                    indices_containing_stock.append(index)
                    break
            
            time.sleep(5)
                        
        return indices_containing_stock

    def find_missing_weight(self, weights_available_list: list, missing_weight_stock: dict):
        """ 
        Takes an list of stocks whose weightage is available and a stock whose weightage is missing and calculates the missing weightage

        :param weights_available_list: List of OrderedDicts representing stocks for which weights are available
        :param missing_weight_stock: OrderedDict representing a stock for which the weightage is missing (None)
        :returns: OrderedDict representing the stock with weightage included
        """
        print('missing_weight_stock: {}'.format(missing_weight_stock))
        if not weights_available_list:
            print("Weight dict doesn't contain any data!")
            return
        if not missing_weight_stock:
            print("Missing stock dict doesn't contain any data!")
            return

        print('weights_available_list_count: {}'.format(len(weights_available_list)))

        available_weights_sum = sum(stock['Weightage'] for stock in weights_available_list)
        print('available_weights_sum: {}'.format(available_weights_sum))

        missing_weight_stock['Weightage'] = round(100 - available_weights_sum, 2)
        print('missing_weight_stock: {}'.format(missing_weight_stock))
 
        return missing_weight_stock

    def get_default_file_directory(self, operation_type: str) -> str:
        home_folder = os.getenv('HOME') if os.getenv('HOME') else os.getenv('USERPROFILE')
        if operation_type.lower() == 'output':
            return os.path.join(home_folder, constants.APP_DIR_NAME, 'output_files')
        elif operation_type.lower() == 'input':
            return os.path.join(home_folder, constants.APP_DIR_NAME, 'input_files')
        else:
            return os.path.join(home_folder, constants.APP_DIR_NAME, 'other_files')